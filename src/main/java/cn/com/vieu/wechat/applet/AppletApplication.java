package cn.com.vieu.wechat.applet;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * @author yinghua.li
 * @date 2019-01-11
 */
@MapperScan("cn.com.vieu.wechat.applet.dao")
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class AppletApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppletApplication.class, args);
    }

}

