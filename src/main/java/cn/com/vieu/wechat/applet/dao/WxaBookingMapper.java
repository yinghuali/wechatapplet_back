package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaBooking;

/**
 * @author yinghua.li
 * @date 2019-01-15 14:41
 */
public interface WxaBookingMapper {
    /**
     * insert into booking
     * @param record 1
     * @return 1
     */
    int insert(WxaBooking record);

    int deleteByPrimaryKey(Integer bookingId);


    int insertSelective(WxaBooking record);

    WxaBooking selectByPrimaryKey(Integer bookingId);

    int updateByPrimaryKeySelective(WxaBooking record);

    int updateByPrimaryKey(WxaBooking record);
}