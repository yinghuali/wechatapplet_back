package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaCity;

import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-14 16:56
 */
public interface WxaCityMapper {

    /**
     * get city list
     * @return 1
     */
    List<Map<String,Object>> list();

    int deleteByPrimaryKey(Integer cityId);

    int insert(WxaCity record);

    int insertSelective(WxaCity record);

    WxaCity selectByPrimaryKey(Integer cityId);

    int updateByPrimaryKeySelective(WxaCity record);

    int updateByPrimaryKey(WxaCity record);
}