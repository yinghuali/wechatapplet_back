package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaRoute;

public interface WxaRouteMapper {
    int deleteByPrimaryKey(Integer routeId);

    int insert(WxaRoute record);

    int insertSelective(WxaRoute record);

    WxaRoute selectByPrimaryKey(Integer routeId);

    int updateByPrimaryKeySelective(WxaRoute record);

    int updateByPrimaryKey(WxaRoute record);
}