package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaStation;

public interface WxaStationMapper {
    int deleteByPrimaryKey(Integer stationId);

    int insert(WxaStation record);

    int insertSelective(WxaStation record);

    WxaStation selectByPrimaryKey(Integer stationId);

    int updateByPrimaryKeySelective(WxaStation record);

    int updateByPrimaryKey(WxaStation record);
}