package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaUser;

public interface WxaUserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(WxaUser record);

    int insertSelective(WxaUser record);

    WxaUser selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(WxaUser record);

    int updateByPrimaryKey(WxaUser record);
}