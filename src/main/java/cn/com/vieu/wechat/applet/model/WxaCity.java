package cn.com.vieu.wechat.applet.model;
/**
 * @author yinghua.li
 * @date 2019-01-11
 */
public class WxaCity {
    private Integer cityId;

    private String name;

    private Boolean isActive;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}