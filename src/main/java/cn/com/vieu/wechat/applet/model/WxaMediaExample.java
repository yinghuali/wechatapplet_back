package cn.com.vieu.wechat.applet.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class WxaMediaExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WxaMediaExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andMediaidIsNull() {
            addCriterion("mediaid is null");
            return (Criteria) this;
        }

        public Criteria andMediaidIsNotNull() {
            addCriterion("mediaid is not null");
            return (Criteria) this;
        }

        public Criteria andMediaidEqualTo(Long value) {
            addCriterion("mediaid =", value, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidNotEqualTo(Long value) {
            addCriterion("mediaid <>", value, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidGreaterThan(Long value) {
            addCriterion("mediaid >", value, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidGreaterThanOrEqualTo(Long value) {
            addCriterion("mediaid >=", value, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidLessThan(Long value) {
            addCriterion("mediaid <", value, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidLessThanOrEqualTo(Long value) {
            addCriterion("mediaid <=", value, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidIn(List<Long> values) {
            addCriterion("mediaid in", values, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidNotIn(List<Long> values) {
            addCriterion("mediaid not in", values, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidBetween(Long value1, Long value2) {
            addCriterion("mediaid between", value1, value2, "mediaid");
            return (Criteria) this;
        }

        public Criteria andMediaidNotBetween(Long value1, Long value2) {
            addCriterion("mediaid not between", value1, value2, "mediaid");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andMediafeeIsNull() {
            addCriterion("mediafee is null");
            return (Criteria) this;
        }

        public Criteria andMediafeeIsNotNull() {
            addCriterion("mediafee is not null");
            return (Criteria) this;
        }

        public Criteria andMediafeeEqualTo(Double value) {
            addCriterion("mediafee =", value, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeNotEqualTo(Double value) {
            addCriterion("mediafee <>", value, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeGreaterThan(Double value) {
            addCriterion("mediafee >", value, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeGreaterThanOrEqualTo(Double value) {
            addCriterion("mediafee >=", value, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeLessThan(Double value) {
            addCriterion("mediafee <", value, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeLessThanOrEqualTo(Double value) {
            addCriterion("mediafee <=", value, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeIn(List<Double> values) {
            addCriterion("mediafee in", values, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeNotIn(List<Double> values) {
            addCriterion("mediafee not in", values, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeBetween(Double value1, Double value2) {
            addCriterion("mediafee between", value1, value2, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediafeeNotBetween(Double value1, Double value2) {
            addCriterion("mediafee not between", value1, value2, "mediafee");
            return (Criteria) this;
        }

        public Criteria andMediacodeIsNull() {
            addCriterion("mediacode is null");
            return (Criteria) this;
        }

        public Criteria andMediacodeIsNotNull() {
            addCriterion("mediacode is not null");
            return (Criteria) this;
        }

        public Criteria andMediacodeEqualTo(String value) {
            addCriterion("mediacode =", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeNotEqualTo(String value) {
            addCriterion("mediacode <>", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeGreaterThan(String value) {
            addCriterion("mediacode >", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeGreaterThanOrEqualTo(String value) {
            addCriterion("mediacode >=", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeLessThan(String value) {
            addCriterion("mediacode <", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeLessThanOrEqualTo(String value) {
            addCriterion("mediacode <=", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeLike(String value) {
            addCriterion("mediacode like", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeNotLike(String value) {
            addCriterion("mediacode not like", value, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeIn(List<String> values) {
            addCriterion("mediacode in", values, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeNotIn(List<String> values) {
            addCriterion("mediacode not in", values, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeBetween(String value1, String value2) {
            addCriterion("mediacode between", value1, value2, "mediacode");
            return (Criteria) this;
        }

        public Criteria andMediacodeNotBetween(String value1, String value2) {
            addCriterion("mediacode not between", value1, value2, "mediacode");
            return (Criteria) this;
        }

        public Criteria andStationidIsNull() {
            addCriterion("stationid is null");
            return (Criteria) this;
        }

        public Criteria andStationidIsNotNull() {
            addCriterion("stationid is not null");
            return (Criteria) this;
        }

        public Criteria andStationidEqualTo(Integer value) {
            addCriterion("stationid =", value, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidNotEqualTo(Integer value) {
            addCriterion("stationid <>", value, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidGreaterThan(Integer value) {
            addCriterion("stationid >", value, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidGreaterThanOrEqualTo(Integer value) {
            addCriterion("stationid >=", value, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidLessThan(Integer value) {
            addCriterion("stationid <", value, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidLessThanOrEqualTo(Integer value) {
            addCriterion("stationid <=", value, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidIn(List<Integer> values) {
            addCriterion("stationid in", values, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidNotIn(List<Integer> values) {
            addCriterion("stationid not in", values, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidBetween(Integer value1, Integer value2) {
            addCriterion("stationid between", value1, value2, "stationid");
            return (Criteria) this;
        }

        public Criteria andStationidNotBetween(Integer value1, Integer value2) {
            addCriterion("stationid not between", value1, value2, "stationid");
            return (Criteria) this;
        }

        public Criteria andHeatIsNull() {
            addCriterion("heat is null");
            return (Criteria) this;
        }

        public Criteria andHeatIsNotNull() {
            addCriterion("heat is not null");
            return (Criteria) this;
        }

        public Criteria andHeatEqualTo(Double value) {
            addCriterion("heat =", value, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatNotEqualTo(Double value) {
            addCriterion("heat <>", value, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatGreaterThan(Double value) {
            addCriterion("heat >", value, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatGreaterThanOrEqualTo(Double value) {
            addCriterion("heat >=", value, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatLessThan(Double value) {
            addCriterion("heat <", value, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatLessThanOrEqualTo(Double value) {
            addCriterion("heat <=", value, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatIn(List<Double> values) {
            addCriterion("heat in", values, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatNotIn(List<Double> values) {
            addCriterion("heat not in", values, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatBetween(Double value1, Double value2) {
            addCriterion("heat between", value1, value2, "heat");
            return (Criteria) this;
        }

        public Criteria andHeatNotBetween(Double value1, Double value2) {
            addCriterion("heat not between", value1, value2, "heat");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Double value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Double value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Double value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Double value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Double value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Double value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Double> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Double> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Double value1, Double value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Double value1, Double value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andDetailsIsNull() {
            addCriterion("details is null");
            return (Criteria) this;
        }

        public Criteria andDetailsIsNotNull() {
            addCriterion("details is not null");
            return (Criteria) this;
        }

        public Criteria andDetailsEqualTo(String value) {
            addCriterion("details =", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsNotEqualTo(String value) {
            addCriterion("details <>", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsGreaterThan(String value) {
            addCriterion("details >", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsGreaterThanOrEqualTo(String value) {
            addCriterion("details >=", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsLessThan(String value) {
            addCriterion("details <", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsLessThanOrEqualTo(String value) {
            addCriterion("details <=", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsLike(String value) {
            addCriterion("details like", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsNotLike(String value) {
            addCriterion("details not like", value, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsIn(List<String> values) {
            addCriterion("details in", values, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsNotIn(List<String> values) {
            addCriterion("details not in", values, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsBetween(String value1, String value2) {
            addCriterion("details between", value1, value2, "details");
            return (Criteria) this;
        }

        public Criteria andDetailsNotBetween(String value1, String value2) {
            addCriterion("details not between", value1, value2, "details");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("flag like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("flag not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andCraftIsNull() {
            addCriterion("craft is null");
            return (Criteria) this;
        }

        public Criteria andCraftIsNotNull() {
            addCriterion("craft is not null");
            return (Criteria) this;
        }

        public Criteria andCraftEqualTo(Integer value) {
            addCriterion("craft =", value, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftNotEqualTo(Integer value) {
            addCriterion("craft <>", value, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftGreaterThan(Integer value) {
            addCriterion("craft >", value, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftGreaterThanOrEqualTo(Integer value) {
            addCriterion("craft >=", value, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftLessThan(Integer value) {
            addCriterion("craft <", value, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftLessThanOrEqualTo(Integer value) {
            addCriterion("craft <=", value, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftIn(List<Integer> values) {
            addCriterion("craft in", values, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftNotIn(List<Integer> values) {
            addCriterion("craft not in", values, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftBetween(Integer value1, Integer value2) {
            addCriterion("craft between", value1, value2, "craft");
            return (Criteria) this;
        }

        public Criteria andCraftNotBetween(Integer value1, Integer value2) {
            addCriterion("craft not between", value1, value2, "craft");
            return (Criteria) this;
        }

        public Criteria andLightstartIsNull() {
            addCriterion("lightstart is null");
            return (Criteria) this;
        }

        public Criteria andLightstartIsNotNull() {
            addCriterion("lightstart is not null");
            return (Criteria) this;
        }

        public Criteria andLightstartEqualTo(Date value) {
            addCriterionForJDBCTime("lightstart =", value, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartNotEqualTo(Date value) {
            addCriterionForJDBCTime("lightstart <>", value, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartGreaterThan(Date value) {
            addCriterionForJDBCTime("lightstart >", value, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("lightstart >=", value, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartLessThan(Date value) {
            addCriterionForJDBCTime("lightstart <", value, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("lightstart <=", value, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartIn(List<Date> values) {
            addCriterionForJDBCTime("lightstart in", values, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartNotIn(List<Date> values) {
            addCriterionForJDBCTime("lightstart not in", values, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("lightstart between", value1, value2, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightstartNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("lightstart not between", value1, value2, "lightstart");
            return (Criteria) this;
        }

        public Criteria andLightendIsNull() {
            addCriterion("lightend is null");
            return (Criteria) this;
        }

        public Criteria andLightendIsNotNull() {
            addCriterion("lightend is not null");
            return (Criteria) this;
        }

        public Criteria andLightendEqualTo(Date value) {
            addCriterionForJDBCTime("lightend =", value, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendNotEqualTo(Date value) {
            addCriterionForJDBCTime("lightend <>", value, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendGreaterThan(Date value) {
            addCriterionForJDBCTime("lightend >", value, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("lightend >=", value, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendLessThan(Date value) {
            addCriterionForJDBCTime("lightend <", value, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("lightend <=", value, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendIn(List<Date> values) {
            addCriterionForJDBCTime("lightend in", values, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendNotIn(List<Date> values) {
            addCriterionForJDBCTime("lightend not in", values, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("lightend between", value1, value2, "lightend");
            return (Criteria) this;
        }

        public Criteria andLightendNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("lightend not between", value1, value2, "lightend");
            return (Criteria) this;
        }

        public Criteria andActualwidthIsNull() {
            addCriterion("actualwidth is null");
            return (Criteria) this;
        }

        public Criteria andActualwidthIsNotNull() {
            addCriterion("actualwidth is not null");
            return (Criteria) this;
        }

        public Criteria andActualwidthEqualTo(Double value) {
            addCriterion("actualwidth =", value, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthNotEqualTo(Double value) {
            addCriterion("actualwidth <>", value, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthGreaterThan(Double value) {
            addCriterion("actualwidth >", value, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthGreaterThanOrEqualTo(Double value) {
            addCriterion("actualwidth >=", value, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthLessThan(Double value) {
            addCriterion("actualwidth <", value, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthLessThanOrEqualTo(Double value) {
            addCriterion("actualwidth <=", value, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthIn(List<Double> values) {
            addCriterion("actualwidth in", values, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthNotIn(List<Double> values) {
            addCriterion("actualwidth not in", values, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthBetween(Double value1, Double value2) {
            addCriterion("actualwidth between", value1, value2, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualwidthNotBetween(Double value1, Double value2) {
            addCriterion("actualwidth not between", value1, value2, "actualwidth");
            return (Criteria) this;
        }

        public Criteria andActualheightIsNull() {
            addCriterion("actualheight is null");
            return (Criteria) this;
        }

        public Criteria andActualheightIsNotNull() {
            addCriterion("actualheight is not null");
            return (Criteria) this;
        }

        public Criteria andActualheightEqualTo(Double value) {
            addCriterion("actualheight =", value, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightNotEqualTo(Double value) {
            addCriterion("actualheight <>", value, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightGreaterThan(Double value) {
            addCriterion("actualheight >", value, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightGreaterThanOrEqualTo(Double value) {
            addCriterion("actualheight >=", value, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightLessThan(Double value) {
            addCriterion("actualheight <", value, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightLessThanOrEqualTo(Double value) {
            addCriterion("actualheight <=", value, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightIn(List<Double> values) {
            addCriterion("actualheight in", values, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightNotIn(List<Double> values) {
            addCriterion("actualheight not in", values, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightBetween(Double value1, Double value2) {
            addCriterion("actualheight between", value1, value2, "actualheight");
            return (Criteria) this;
        }

        public Criteria andActualheightNotBetween(Double value1, Double value2) {
            addCriterion("actualheight not between", value1, value2, "actualheight");
            return (Criteria) this;
        }

        public Criteria andDesignwidthIsNull() {
            addCriterion("designwidth is null");
            return (Criteria) this;
        }

        public Criteria andDesignwidthIsNotNull() {
            addCriterion("designwidth is not null");
            return (Criteria) this;
        }

        public Criteria andDesignwidthEqualTo(Double value) {
            addCriterion("designwidth =", value, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthNotEqualTo(Double value) {
            addCriterion("designwidth <>", value, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthGreaterThan(Double value) {
            addCriterion("designwidth >", value, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthGreaterThanOrEqualTo(Double value) {
            addCriterion("designwidth >=", value, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthLessThan(Double value) {
            addCriterion("designwidth <", value, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthLessThanOrEqualTo(Double value) {
            addCriterion("designwidth <=", value, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthIn(List<Double> values) {
            addCriterion("designwidth in", values, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthNotIn(List<Double> values) {
            addCriterion("designwidth not in", values, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthBetween(Double value1, Double value2) {
            addCriterion("designwidth between", value1, value2, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignwidthNotBetween(Double value1, Double value2) {
            addCriterion("designwidth not between", value1, value2, "designwidth");
            return (Criteria) this;
        }

        public Criteria andDesignheightIsNull() {
            addCriterion("designheight is null");
            return (Criteria) this;
        }

        public Criteria andDesignheightIsNotNull() {
            addCriterion("designheight is not null");
            return (Criteria) this;
        }

        public Criteria andDesignheightEqualTo(Double value) {
            addCriterion("designheight =", value, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightNotEqualTo(Double value) {
            addCriterion("designheight <>", value, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightGreaterThan(Double value) {
            addCriterion("designheight >", value, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightGreaterThanOrEqualTo(Double value) {
            addCriterion("designheight >=", value, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightLessThan(Double value) {
            addCriterion("designheight <", value, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightLessThanOrEqualTo(Double value) {
            addCriterion("designheight <=", value, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightIn(List<Double> values) {
            addCriterion("designheight in", values, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightNotIn(List<Double> values) {
            addCriterion("designheight not in", values, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightBetween(Double value1, Double value2) {
            addCriterion("designheight between", value1, value2, "designheight");
            return (Criteria) this;
        }

        public Criteria andDesignheightNotBetween(Double value1, Double value2) {
            addCriterion("designheight not between", value1, value2, "designheight");
            return (Criteria) this;
        }

        public Criteria andVisualwidthIsNull() {
            addCriterion("visualwidth is null");
            return (Criteria) this;
        }

        public Criteria andVisualwidthIsNotNull() {
            addCriterion("visualwidth is not null");
            return (Criteria) this;
        }

        public Criteria andVisualwidthEqualTo(Double value) {
            addCriterion("visualwidth =", value, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthNotEqualTo(Double value) {
            addCriterion("visualwidth <>", value, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthGreaterThan(Double value) {
            addCriterion("visualwidth >", value, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthGreaterThanOrEqualTo(Double value) {
            addCriterion("visualwidth >=", value, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthLessThan(Double value) {
            addCriterion("visualwidth <", value, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthLessThanOrEqualTo(Double value) {
            addCriterion("visualwidth <=", value, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthIn(List<Double> values) {
            addCriterion("visualwidth in", values, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthNotIn(List<Double> values) {
            addCriterion("visualwidth not in", values, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthBetween(Double value1, Double value2) {
            addCriterion("visualwidth between", value1, value2, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualwidthNotBetween(Double value1, Double value2) {
            addCriterion("visualwidth not between", value1, value2, "visualwidth");
            return (Criteria) this;
        }

        public Criteria andVisualheightIsNull() {
            addCriterion("visualheight is null");
            return (Criteria) this;
        }

        public Criteria andVisualheightIsNotNull() {
            addCriterion("visualheight is not null");
            return (Criteria) this;
        }

        public Criteria andVisualheightEqualTo(Double value) {
            addCriterion("visualheight =", value, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightNotEqualTo(Double value) {
            addCriterion("visualheight <>", value, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightGreaterThan(Double value) {
            addCriterion("visualheight >", value, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightGreaterThanOrEqualTo(Double value) {
            addCriterion("visualheight >=", value, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightLessThan(Double value) {
            addCriterion("visualheight <", value, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightLessThanOrEqualTo(Double value) {
            addCriterion("visualheight <=", value, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightIn(List<Double> values) {
            addCriterion("visualheight in", values, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightNotIn(List<Double> values) {
            addCriterion("visualheight not in", values, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightBetween(Double value1, Double value2) {
            addCriterion("visualheight between", value1, value2, "visualheight");
            return (Criteria) this;
        }

        public Criteria andVisualheightNotBetween(Double value1, Double value2) {
            addCriterion("visualheight not between", value1, value2, "visualheight");
            return (Criteria) this;
        }

        public Criteria andCityidIsNull() {
            addCriterion("cityid is null");
            return (Criteria) this;
        }

        public Criteria andCityidIsNotNull() {
            addCriterion("cityid is not null");
            return (Criteria) this;
        }

        public Criteria andCityidEqualTo(Integer value) {
            addCriterion("cityid =", value, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidNotEqualTo(Integer value) {
            addCriterion("cityid <>", value, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidGreaterThan(Integer value) {
            addCriterion("cityid >", value, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cityid >=", value, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidLessThan(Integer value) {
            addCriterion("cityid <", value, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidLessThanOrEqualTo(Integer value) {
            addCriterion("cityid <=", value, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidIn(List<Integer> values) {
            addCriterion("cityid in", values, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidNotIn(List<Integer> values) {
            addCriterion("cityid not in", values, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidBetween(Integer value1, Integer value2) {
            addCriterion("cityid between", value1, value2, "cityid");
            return (Criteria) this;
        }

        public Criteria andCityidNotBetween(Integer value1, Integer value2) {
            addCriterion("cityid not between", value1, value2, "cityid");
            return (Criteria) this;
        }

        public Criteria andRouteidIsNull() {
            addCriterion("routeid is null");
            return (Criteria) this;
        }

        public Criteria andRouteidIsNotNull() {
            addCriterion("routeid is not null");
            return (Criteria) this;
        }

        public Criteria andRouteidEqualTo(Integer value) {
            addCriterion("routeid =", value, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidNotEqualTo(Integer value) {
            addCriterion("routeid <>", value, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidGreaterThan(Integer value) {
            addCriterion("routeid >", value, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidGreaterThanOrEqualTo(Integer value) {
            addCriterion("routeid >=", value, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidLessThan(Integer value) {
            addCriterion("routeid <", value, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidLessThanOrEqualTo(Integer value) {
            addCriterion("routeid <=", value, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidIn(List<Integer> values) {
            addCriterion("routeid in", values, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidNotIn(List<Integer> values) {
            addCriterion("routeid not in", values, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidBetween(Integer value1, Integer value2) {
            addCriterion("routeid between", value1, value2, "routeid");
            return (Criteria) this;
        }

        public Criteria andRouteidNotBetween(Integer value1, Integer value2) {
            addCriterion("routeid not between", value1, value2, "routeid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}