package cn.com.vieu.wechat.applet.model;
/**
 * @author yinghua.li
 * @date 2019-01-11
 */
public class WxaStation {
    private Integer stationId;

    private String name;

    private Integer routeId;

    private Integer rank;

    private Double passengerFlow;

    private Boolean isActive;

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Double getPassengerFlow() {
        return passengerFlow;
    }

    public void setPassengerFlow(Double passengerFlow) {
        this.passengerFlow = passengerFlow;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}