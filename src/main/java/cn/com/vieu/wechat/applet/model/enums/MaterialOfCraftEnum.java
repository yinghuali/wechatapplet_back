package cn.com.vieu.wechat.applet.model.enums;
/**
 * @author yinghua.li
 * @date 2018-11-20 10:36
 */
public enum MaterialOfCraftEnum {
    /**
     *
     */
    CRAFT_1(1,"35C的塑胶PVC,裱背胶"),
    CRAFT_2(2,"紫光正喷灯面+双面覆哑膜/12PASS");

    private int id;
    private String name;

    MaterialOfCraftEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
