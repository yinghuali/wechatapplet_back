package cn.com.vieu.wechat.applet.model.enums;

/**
 * @author yinghua.li
 * @date 2018-11-20 10:36
 */
public enum PackageEnum {
    /**
     * 
     */
    package_1(1,"基础款1天"),

    package_2(2,"豪华款3天");

    PackageEnum(int id,String packageName) {
        this.id = id;
        this.packageName = packageName;
    }

    private int id;
    private String packageName;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}

