package cn.com.vieu.wechat.applet.model.enums;

/**
 * @author yinghua.li
 * @date 2018-12-03 14:17
 */
public enum  StationRankEnum {
    /**
     *
     */
    S(1,"S"),

    AAA(2,"A++"),

    AA(3,"A+"),

    A(4,"A");


    private final int id;
    private  String name;

    StationRankEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
