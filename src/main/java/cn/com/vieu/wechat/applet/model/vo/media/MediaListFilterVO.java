package cn.com.vieu.wechat.applet.model.vo.media;

import java.util.Arrays;
import java.util.Date;

/**
 * @author hua
 */
public class MediaListFilterVO {
    private Integer page;
    private Integer firstResult;
    private Integer pageSize;
    private String keyword;
    private Date[] startEndDate;
    private Integer cityID;
    private Date startDate;
    private Date endDate;
    /**
     * 0-热度推荐 1-最新上架
     */
    private Integer orderBy;

    /**
     * 0(1000-2000) 1(2000-3000) 2(3000以上)
     */
    private Integer mediaFee;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Date[] getStartEndDate() {
        return startEndDate;
    }

    public void setStartEndDate(Date[] startEndDate) {
        this.startEndDate = startEndDate;
    }

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getMediaFee() {
        return mediaFee;
    }

    public void setMediaFee(Integer mediaFee) {
        this.mediaFee = mediaFee;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "MediaListFilterVO{" +
                "page=" + page +
                ", firstResult=" + firstResult +
                ", pageSize=" + pageSize +
                ", keyword='" + keyword + '\'' +
                ", startEndDate=" + Arrays.toString(startEndDate) +
                ", cityID=" + cityID +
                ", orderBy=" + orderBy +
                ", mediaFee=" + mediaFee +
                '}';
    }
}
