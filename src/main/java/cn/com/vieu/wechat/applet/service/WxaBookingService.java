package cn.com.vieu.wechat.applet.service;

import cn.com.vieu.wechat.applet.model.WxaBooking;

/**
 * @author yinghua.li
 * @date 2019-01-15 14:40
 */
public interface WxaBookingService {

    /**
     * insert into booking
     * @param record 1
     * @return 1
     */
    int insert(WxaBooking record);

}
