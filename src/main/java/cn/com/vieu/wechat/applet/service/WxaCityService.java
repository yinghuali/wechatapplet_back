package cn.com.vieu.wechat.applet.service;

import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-14 16:57
 */
public interface WxaCityService {

    /**
     * get city list
     * @return
     */
    List<Map<String,Object>> list();
}
