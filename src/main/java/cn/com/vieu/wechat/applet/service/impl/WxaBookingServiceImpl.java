package cn.com.vieu.wechat.applet.service.impl;

import cn.com.vieu.wechat.applet.dao.WxaBookingMapper;
import cn.com.vieu.wechat.applet.model.WxaBooking;
import cn.com.vieu.wechat.applet.service.WxaBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yinghua.li
 * @date 2019-01-15 14:40
 */
@Service("wxaBookingService")
public class WxaBookingServiceImpl implements WxaBookingService {

    @Autowired
    private WxaBookingMapper wxaBookingMapper;

    @Override
    public int insert(WxaBooking record) {
        return wxaBookingMapper.insert(record);
    }
}
