package cn.com.vieu.wechat.applet.service.impl;

import cn.com.vieu.wechat.applet.dao.WxaCityMapper;
import cn.com.vieu.wechat.applet.service.WxaCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @author yinghua.li
 * @date 2019-01-14 16:57
 */
@Service("wxaCityService")
public class WxaCityServiceImpl implements WxaCityService {

    @Autowired
    private WxaCityMapper wxaCityMapper;

    @Override
    public List<Map<String, Object>> list() {
        return wxaCityMapper.list();
    }
}
