package cn.com.vieu.wechat.applet.service.wx.impl;

import cn.com.vieu.wechat.applet.util.HttpUtil;
import cn.com.vieu.wechat.applet.util.RedisLock;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * @author hua
 */
@Service
public class WxContextService extends RedisOperationAbstract {
    private static final Logger logger = LoggerFactory.getLogger(WxContextService.class);
    private static final String APP_ID = "wx14d4582c89175533";
    private static final String SECRET = "21d33eb1fdf80405e798c8789d3820c6";
    private static final String TOKEN = "2921b2591f81409db09f48cdd1d13385";
    private static final String ENCODING_AES_KEY = "pxnZmKRjLrCZSY4U5hQkv7VmFfN67FuHtj5xFMjLIrE";
    private static final String REDIS_TOKEN_KEY = "key_emp_wx_access_token";
    private static final String REDIS_TOKEN_LOCK = "lock_emp_wx_access_token";
    /**
     * 获取access_token GET
     */
    private static final String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";
    private static final String URL_GET_USER_OPENID = "https://api.weixin.qq.com/sns/jscode2session";

    private String getWxAccessToken() {
        Map<String, String> params = new HashMap<>();
        params.put("grant_type", "client_credential");
        params.put("appid", APP_ID);
        params.put("secret", SECRET);
        String resultStr = HttpUtil.get(URL_ACCESS_TOKEN, params);
        JSONObject result = JSON.parseObject(resultStr);
        if (result.getString("errcode") != null) {
            logger.error("获取微信token失败:{}", result.toJSONString());
            return null;
        } else {
            logger.debug("获取微信token成功");
            return result.getString("access_token");
        }
    }

    /**
     *通过凭证code换取用户登录态信息，包括用户的唯一标识（openid）及本次登录的会话密钥（session_key）等
     * @param code 1
     * @return 1
     */
    public String getUserOpenID(String code){
        Map<String, String> params = new HashMap<>(2);
        params.put("appid", APP_ID);
        params.put("secret", SECRET);
        params.put("js_code", code);
        params.put("grant_type","authorization_code");
        return HttpUtil.get(URL_GET_USER_OPENID, params);
    }

    /**
     * 获取微信token
     * @return 1
     */
    public String accessToken() {
        RedisLock redisLock = new RedisLock(redisTemplate, REDIS_TOKEN_LOCK);
        try {
            redisLock.lock();
            Object o = kvOpt().get(REDIS_TOKEN_KEY);
            if (o == null) {
                //Redis7000秒超时
                String wxAccessToken = getWxAccessToken();
                kvOpt().set(REDIS_TOKEN_KEY, wxAccessToken, 60, TimeUnit.SECONDS);
                return wxAccessToken;
            } else {
                return o.toString();
            }
        } catch (Exception e) {
            logger.error("获取缓存token时发生异常", e);
            return null;
        } finally {
            redisLock.unlock();
        }
    }
}
