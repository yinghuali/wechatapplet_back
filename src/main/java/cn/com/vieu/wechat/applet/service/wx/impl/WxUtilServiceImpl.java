package cn.com.vieu.wechat.applet.service.wx.impl;


import cn.com.vieu.wechat.applet.service.wx.IWxUtilService;
import cn.com.vieu.wechat.applet.util.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author yinghua.li
 */
@Service
public class WxUtilServiceImpl implements IWxUtilService {
    private static final Logger logger = LoggerFactory.getLogger(WxUtilServiceImpl.class);
    @Resource
    private WxContextService wxContextService;

}
