package cn.com.vieu.wechat.applet.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * @author admin
 * @date 2019-01-14 10:59
 */
@Scope("request")
public abstract class BaseAction {
    private static Logger logger = LogManager.getLogger(BaseAction.class);

    private HttpServletRequest request;

    protected Integer resultStatus =ResultCode.SUCCESS.code;


    protected boolean validateRequestFieldIsNullOrEmpty(Object... fields) {
        if (fields == null || fields.length == 0) {
            return true;
        }

        for (Object field : fields) {
            if (field == null) {
                return true;
            } else if (field instanceof String && ((String) field).isEmpty()) {
                return true;
            } else if (field instanceof Collection && ((Collection) field).isEmpty()) {
                return true;
            }
        }

        return false;
    }

    protected int parsePageNumber(Object page,Integer pageSize) {
        if (page == null) {
            return 0;
        }

        if(pageSize == null){
            pageSize=10;
        }

        if (page instanceof Integer) {
            Integer pageInt = (Integer) page;
            if (pageInt < 1) {
                return 0;
            }
            return (pageInt - 1) * pageSize;
        }else if (page instanceof String && !((String)page).isEmpty()) {
            try {
                int result = Integer.parseInt((String)page);
                if (result < 1) {
                    return 0;
                }

                return (result - 1) * pageSize;
            }catch (NumberFormatException e) {
                return 0;
            }
        }

        return 0;
    }

    public void setCurrentHttpServletRequest (HttpServletRequest request) {
        this.request = request;
    }

}
