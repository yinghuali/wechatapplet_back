package cn.com.vieu.wechat.applet.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateParser {
	private static final Logger logger = LogManager.getLogger(DateParser.class);

	private static final SimpleDateFormat dfFull;
	private static final SimpleDateFormat dfDate;
	static {
		dfFull = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		dfDate = new SimpleDateFormat("yyyy-MM-dd");
	}
	private DateParser(){}

	@Deprecated
	public static boolean validateDate(Date date) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		return validateDate(calender.get(Calendar.YEAR), calender.get(Calendar.MONTH)+1, calender.get(Calendar.DAY_OF_MONTH));
	}

	@Deprecated
	public static boolean validateDate(int year, int month, int day) {
		int judge = 0;
		
		switch (month) {
		case 1:case 3:case 5:case 7:case 8:case 10:case 12:
			if (0 < day && day < 32) {
				judge = 1;
			}
			break;
		case 4:case 6:case 9:case 11:
			if (0 < day && day < 31) {
				judge = 1;
			}
			break;
		case 2:
			if ((year%4 == 0 && year%100 != 0) || year%400 == 0) {
				if (0 < day && day < 30) {
					judge = 1;
				}
			}else {
				if (0 < day && day < 29) {
					judge = 1;
				}
			}
			break;
		}
		
		if (judge == 1) {
			return true;
		}
		return false;
	}
	
	public static Date parseTimeStamp(String timestamp) {
		logger.info("parseTimeStamp(" + timestamp + ")");
		if (timestamp == null || timestamp.isEmpty()) {
			return null;
		}

		try {
			synchronized (dfFull) {
				return dfFull.parse(timestamp.substring(0,19));
			}
		} catch (Exception e) {
			try {
				Long timeMillis = Long.parseLong(timestamp);
				return new Date(timeMillis);
			} catch (Exception e1) {
				logger.error("parse timestamp error please check input time string");
				return null;
			}
		}
	}

	public static Date parseDate(String timestamp) {
		logger.info("parseTimeStampDate(" + timestamp + ")");
		if (timestamp == null || timestamp.isEmpty()) {
			return null;
		}

		try {
			synchronized (dfDate) {
				return dfDate.parse(timestamp.substring(0,10));
			}
		} catch (ParseException e) {
			logger.error("parse timestamp failed : " + e.getMessage());
			return null;
		}
	}

	public static Date getCurrentUTCDate() {
		return getCurrentUTCDate(null, null, null, null, null, null);
	}

	public static Date getCurrentUTCDate(Integer year, Integer month, Integer day, Integer hour, Integer minute, Integer second) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.setTime(new Date());
		if (year != null) {
			calendar.add(Calendar.YEAR, year);
		}
		if (month != null) {
			calendar.add(Calendar.MONTH, month);
		}
		if (day != null) {
			calendar.add(Calendar.DAY_OF_MONTH, day);
		}
		if (hour != null) {
			calendar.add(Calendar.HOUR_OF_DAY, hour);
		}
		if (minute != null) {
			calendar.add(Calendar.MINUTE, minute);
		}
		if (second != null) {
			calendar.add(Calendar.SECOND, second);
		}

		StringBuilder currentUTCTimeStampBuilder = new StringBuilder();
		currentUTCTimeStampBuilder.append(calendar.get(Calendar.YEAR));
		currentUTCTimeStampBuilder.append("-");

		int calMonth = (calendar.get(Calendar.MONTH) + 1);
		if (1 <= calMonth && calMonth <= 9) {
			currentUTCTimeStampBuilder.append("0" + calMonth);
		}else {
			currentUTCTimeStampBuilder.append(calMonth);
		}
		currentUTCTimeStampBuilder.append("-");

		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		if (1 <= dayOfMonth && dayOfMonth <= 9) {
			currentUTCTimeStampBuilder.append("0" + dayOfMonth);
		}else {
			currentUTCTimeStampBuilder.append(dayOfMonth);
		}
		currentUTCTimeStampBuilder.append("T");

		int calHour = calendar.get(Calendar.HOUR_OF_DAY);
		if (0 <= calHour && calHour <= 9) {
			currentUTCTimeStampBuilder.append("0" + calHour);
		}else {
			currentUTCTimeStampBuilder.append(calHour);
		}
		currentUTCTimeStampBuilder.append(":");

		int calMinute = calendar.get(Calendar.MINUTE);
		if (0 <= calMinute && calMinute <= 9) {
			currentUTCTimeStampBuilder.append("0" + calMinute);
		}else {
			currentUTCTimeStampBuilder.append(calMinute);
		}
		currentUTCTimeStampBuilder.append(":");

		int calSecond = calendar.get(Calendar.SECOND);
		if (0 <= calSecond && calSecond <= 9) {
			currentUTCTimeStampBuilder.append("0" + calSecond);
		}else {
			currentUTCTimeStampBuilder.append(calSecond);
		}

		String currentUTCTimeStamp = currentUTCTimeStampBuilder.toString();
		logger.info("current UTC time stamp : " + currentUTCTimeStamp);

		return parseTimeStamp(currentUTCTimeStamp);
	}

	public static Calendar getCalendar(Date date) {
		return getCalendar(date, null, null, null ,null, null, null, null);
	}

	public static Calendar getCalendarUTC(Date date) {
		return getCalendar(date, TimeZone.getTimeZone("UTC"), null, null ,null, null, null, null);
	}

	public static Calendar getCalendar(Date date, TimeZone timeZone, Integer year, Integer month, Integer day, Integer hour, Integer minute, Integer second) {
		if (date == null) {
			return null;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (timeZone != null) {
			calendar.setTimeZone(timeZone);
		}

		if (year != null) {
			calendar.add(Calendar.YEAR, year);
		}
		if (month != null) {
			calendar.add(Calendar.MONTH, month);
		}
		if (day != null) {
			calendar.add(Calendar.DAY_OF_MONTH, day);
		}
		if (hour != null) {
			calendar.add(Calendar.HOUR_OF_DAY, hour);
		}
		if (minute != null) {
			calendar.add(Calendar.MINUTE, minute);
		}
		if (second != null) {
			calendar.add(Calendar.SECOND, second);
		}

		return calendar;
	}

	public static String getCalendarDateString(Calendar calendar, String separator) {
		if (calendar == null || separator == null || separator.isEmpty()) {
			return null;
		}

		int mon = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);

		StringBuilder builder = new StringBuilder();
		builder.append(calendar.get(Calendar.YEAR));
		builder.append(separator);
		builder.append(mon < 10 ? ("0" + mon) : mon);
		builder.append(separator);
		builder.append(day < 10 ? ("0" + day) : day);

		return builder.toString();
	}

	public static String getCalendarDateTimeString(Calendar calendar, String separatorDate, String separatorTime, boolean withTimeZone) {
		if (calendar == null || separatorDate == null || separatorDate.isEmpty() || separatorTime == null || separatorTime.isEmpty()) {
			return null;
		}

		int mon = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);

		StringBuilder builder = new StringBuilder();
		builder.append(calendar.get(Calendar.YEAR));
		builder.append(separatorDate);
		builder.append(mon < 10 ? ("0" + mon) : mon);
		builder.append(separatorDate);
		builder.append(day < 10 ? ("0" + day) : day);
		builder.append(withTimeZone == true ? "T" : " ");
		builder.append(hour < 10 ? ("0" + hour) : hour);
		builder.append(separatorTime);
		builder.append(min < 10 ? ("0" + min) : min);
		builder.append(separatorTime);
		builder.append(sec < 10 ? ("0" + sec) : sec);
		builder.append(withTimeZone == true ? "Z" : "");

		return builder.toString();
	}

    public static Date getAddMonthsUTCDate(int months) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(new Date());

        StringBuilder currentUTCTimeStampBuilder = new StringBuilder();
        currentUTCTimeStampBuilder.append(calendar.get(Calendar.YEAR));
        currentUTCTimeStampBuilder.append("-");

        int calMonth = (calendar.get(Calendar.MONTH) + 1+months);
        if (1 <= calMonth && calMonth <= 9) {
            currentUTCTimeStampBuilder.append("0" + calMonth);
        }else {
            currentUTCTimeStampBuilder.append(calMonth);
        }
        currentUTCTimeStampBuilder.append("-");

        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        if (1 <= dayOfMonth && dayOfMonth <= 9) {
            currentUTCTimeStampBuilder.append("0" + dayOfMonth);
        }else {
            currentUTCTimeStampBuilder.append(dayOfMonth);
        }
        currentUTCTimeStampBuilder.append("T");

        int calHour = calendar.get(Calendar.HOUR_OF_DAY);
        if (0 <= calHour && calHour <= 9) {
            currentUTCTimeStampBuilder.append("0" + calHour);
        }else {
            currentUTCTimeStampBuilder.append(calHour);
        }
        currentUTCTimeStampBuilder.append(":");

        int calMinute = calendar.get(Calendar.MINUTE);
        if (0 <= calMinute && calMinute <= 9) {
            currentUTCTimeStampBuilder.append("0" + calMinute);
        }else {
            currentUTCTimeStampBuilder.append(calMinute);
        }
        currentUTCTimeStampBuilder.append(":");

        int calSecond = calendar.get(Calendar.SECOND);
        if (0 <= calSecond && calSecond <= 9) {
            currentUTCTimeStampBuilder.append("0" + calSecond);
        }else {
            currentUTCTimeStampBuilder.append(calSecond);
        }

        String currentUTCTimeStamp = currentUTCTimeStampBuilder.toString();
        logger.info("current UTC time stamp : " + currentUTCTimeStamp);

        return parseTimeStamp(currentUTCTimeStamp);
    }
//	public static String getCalendarDateTimeTimeStampString(Calendar calendar) {
//		if (calendar == null) {
//			return null;
//		}
//
//		int mon = calendar.get(Calendar.MONTH) + 1;
//		int day = calendar.get(Calendar.DAY_OF_MONTH);
//		int hour = calendar.get(Calendar.HOUR_OF_DAY);
//		int min = calendar.get(Calendar.MINUTE);
//		int sec = calendar.get(Calendar.SECOND);
//
//		StringBuilder builder = new StringBuilder();
//		builder.append(calendar.get(Calendar.YEAR));
//		builder.append("-");
//		builder.append(mon < 10 ? ("0" + mon) : mon);
//		builder.append("-");
//		builder.append(day < 10 ? ("0" + day) : day);
//		builder.append("T");
//		builder.append(hour < 10 ? ("0" + hour) : hour);
//		builder.append(":");
//		builder.append(min < 10 ? ("0" + min) : min);
//		builder.append(":");
//		builder.append(sec < 10 ? ("0" + sec) : sec);
//		builder.append("Z");
//
//		return builder.toString();
//	}

	/**
	 * 时间字符串转为java.sql.Time
	 * @param strDate 08:00
	 *
	 */
	public static Time strToDate(String strDate) {
		if("".equals(strDate)||strDate == null){
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat("hh:mm");
		Date d = null;
		try {
			d = format.parse(strDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Time date = new Time(d.getTime());
		return date;
	}

	/**
	 * yyyy/yy/dd transfer to date form
	 * @param strDate
	 * @return
	 */
	public static Date stringToDate(String strDate) throws ParseException {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date parse = format.parse(strDate);

		return parse;
	}
}
