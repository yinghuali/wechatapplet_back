package cn.com.vieu.wechat.applet.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2018-11-20 15:43
 */
public class EnumUtil {


    private final static String METHOD_GET = "get";

    /**
     * get Enum by field
     * @param value
     * @param fileName
     * @param clazz
     * @param <T>
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     */
    public static  <T extends Enum>Enum getEnumById(Object value, String fileName,Class<T> clazz) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        if (!clazz.isEnum()) {
            return null;
        }

        T result = null;

        Method targetMethod = clazz.getDeclaredMethod(METHOD_GET + fileName.substring(0, 1).toUpperCase() + fileName.substring(1));

        Object idVal = null;
        T[] objs = clazz.getEnumConstants();
        for(T entity:objs){
            idVal =  targetMethod.invoke(entity);
            if(idVal.equals(value)){
                result = entity;
                break;
            }
        }
        return result;
    }

    /**
     * 获取枚举所有值的属性
     *
     * @param clazz
     * @return
     */
    public static <T> List<Map<String, Object>> getEnumValues(Class<T> clazz) {
        if (!clazz.isEnum()) {
            return null;
        }

        // 获取枚举的属性字段和相应的get方法
        Map<String, String> propData = new HashMap<>(2);
        for (Field field : clazz.getDeclaredFields()) {
            if (field.getModifiers() == 2 || field.getModifiers() == 18) {
                propData.put(field.getName(), METHOD_GET + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
            }
        }

        List<Map<String, Object>> dataList = new ArrayList<>();
        T[] objs = clazz.getEnumConstants();
        for (int i = 0; i < objs.length; i++) {
            Map<String, Object> data = new HashMap<>(2);
            for (Map.Entry<String, String> entry : propData.entrySet()) {
                try {
                    Method method = clazz.getMethod(entry.getValue());
                    data.put(entry.getKey(), method.invoke(objs[i]));
                } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            dataList.add(data);
        }

        return dataList;
    }
}
