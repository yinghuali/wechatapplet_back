package cn.com.vieu.wechat.applet.util;

import com.jcabi.http.Request;
import com.jcabi.http.request.JdkRequest;
import com.jcabi.http.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.util.Map;

/**
 * http工具类
 *
 * @author hua
 * */
public class HttpUtil {
    private static final Logger logger= LoggerFactory.getLogger(HttpUtil.class);
    public static  String postWithBody(String url,String body){
        try {
            return new JdkRequest(url)
                    .uri()
                    .back()
                    .method(Request.POST)
                    .body().set(body)
                    .back()
                    .header("Content-Type", "application/json")
                    .header("User-Agent", "CRM-SALE")
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
        }

        return null;
    }


    static  String postWithBodyAndAccessToken(String url, String body, String accessToken){
        try {
            return new JdkRequest(url)
                    .uri().queryParam("access_token",accessToken)
                    .back()
                    .method(Request.POST)
                    .body().set(body)
                    .back()
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
        }
        return null;
    }

    public static  String postWithParams(String url,Map<String,String> params){
        try {
            return new JdkRequest(url)
                    .uri().queryParams(params)
                    .back()
                    .method(Request.POST)
                    .header("Content-Type", "application/json")
                    .header("User-Agent", "CRM-SALE")
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
        }
        return null;
    }


    public static  String  get(String url, Map<String, String> params){
        try {
            return new JdkRequest(url)
                    .uri().queryParams(params)
                    .back()
                    .method(Request.GET)
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
            return null;
        }
    }
}
