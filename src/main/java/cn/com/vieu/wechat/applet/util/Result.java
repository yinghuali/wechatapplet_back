package cn.com.vieu.wechat.applet.util;

/**
 * @author yinghua.li
 * @date 2019-01-12 10:13
 * 统一API响应结果封装
 */
public class Result {
    private int code;
    private String message;
    private Object data;

    public Result() {
        this.code = ResultCode.NO_DATA.code;
    }

    public Result(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result setCode(ResultCode resultCode) {
        this.code = resultCode.code;
        return this;
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        /*if ((null == data) ||
                ((data instanceof Collection) && ((Collection) data).size() == 0) ||
                (data instanceof PageInfo) && (null == ((PageInfo) data).getList()) ||
                (data instanceof PageInfo) && (0 == ((PageInfo) data).getSize())) {
            this.code = ResultCode.NO_DATA.code;
            this.message = ResultCode.NO_DATA.msg;
            this.data = null;
        } else {
            this.data = data;
        }*/
        this.data = data;
        return this;
    }

}
