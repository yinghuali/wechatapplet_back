package cn.com.vieu.wechat.applet.util;

/**
 * @author yinghua.li
 * @date 2019-01-12 10：12
 * 响应码枚举，参考HTTP状态码的语义
 */
public enum ResultCode {
    /**
     *成功
     */
    SUCCESS(200,"请求成功"),
    /**
     * 失败
     * FAIL(-1,"请求失败"),
     */


    MISSING_FIELD(-1,"缺少字段"),
    /**
     * 未认证（签名错误）
     */
    UNAUTHORIZED(401,"认证失败"),
    /**
     * 未认证（签名错误）
     */
    NO_CLASS_TICKET(402,"未携带班级认证"),
    /**
     * 接口不存在
     */
    NOT_FOUND(404,"不存在的资源"),
    /**
     * 服务器内部错误
     */
    INTERNAL_SERVER_ERROR(500,"服务器内部错误"),


    /*******自定义*********/
    NO_DATA(606,"没有数据"),

    EXCEPTION(-4,"异常");

    public int code;
    public String msg;

    ResultCode(int code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
