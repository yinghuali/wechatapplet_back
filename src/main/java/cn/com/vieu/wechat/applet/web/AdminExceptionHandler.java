package cn.com.vieu.wechat.applet.web;

import cn.com.vieu.wechat.applet.util.Result;
import cn.com.vieu.wechat.applet.util.ResultCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yinghua.li
 * @date 2018-12-20
 */
@ControllerAdvice
public class AdminExceptionHandler {

        private static final Logger logger = LogManager.getLogger(AdminExceptionHandler.class);

        /**
         * 系统异常捕获处理
         * @param ex 1
         * @return 1
         */
        @ResponseBody
        @ExceptionHandler(value = Exception.class)
        public Result javaExceptionHandler(Exception ex) {//APIResponse是项目中对外统一的出口封装，可以根据自身项目的需求做相应更改
            logger.error("捕获到Exception异常",ex);
            //异常日志入库

            return new Result(ResultCode.EXCEPTION.code,ex.getMessage()==null?ex.toString():ex.getMessage(), null);
        }

}
