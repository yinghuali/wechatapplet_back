package cn.com.vieu.wechat.applet.web;

import cn.com.vieu.wechat.applet.model.WxaBooking;
import cn.com.vieu.wechat.applet.service.WxaBookingService;
import cn.com.vieu.wechat.applet.service.wx.impl.WxContextService;
import cn.com.vieu.wechat.applet.util.BaseAction;
import cn.com.vieu.wechat.applet.util.DateParser;
import cn.com.vieu.wechat.applet.util.Result;
import cn.com.vieu.wechat.applet.util.ResultCode;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yinghua.li
 * @date 2019-01-15 10:20
 */
@Scope("request")
@RequestMapping(value = "/booking", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RestController
public class WxBookingController extends BaseAction {

    @Autowired
    private WxContextService wxContextService;
    @Autowired
    private WxaBookingService wxaBookingService;
    @Value("${common.filePath.data}")
    private String filePath;

    @RequestMapping("/getOpenID")
    public Result getUserOpenID(HttpServletRequest request,HttpServletResponse response,String code){
        Result result = new Result(resultStatus,null,null);
        String resultJson = wxContextService.getUserOpenID(code);
        JSONObject jsonObject = JSON.parseObject(resultJson);
        if(jsonObject.getString("errcode") != null){
            result.setCode(ResultCode.UNAUTHORIZED);
            result.setMessage(jsonObject.getString("errmsg"));
        }else {
            result.setData(jsonObject);
        }

        return result;
    }

    @RequestMapping("/create")
    public Result create(HttpServletRequest request, HttpServletResponse response,
                         WxaBooking wxaBooking,
                         @RequestParam(value = "file") MultipartFile file){
        Result result = new Result(resultStatus,null,null);
        wxaBooking.setStatus(0);
        wxaBooking.setCreatedOn(DateParser.getCurrentUTCDate());
        int insert = wxaBookingService.insert(wxaBooking);

        return result;
    }
}
